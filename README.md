Workbench 404 redirection
=========================

About this module
-----------------
Workbench 404 redirection provides basic support for content entities. That is, it allows
site administrators to configure the define States (that content can be in), for default redirection from 403 access denied to 404 Page not found.

Also as per SEO and end user perspective, content which is set to 'Archived' or in some other state should result in 404 rather than 403.

Installation
------------

Workbench 404 redirection has no special installation instructions. No default configuration ships with the module, you need to configure
the states for which you want redirection or exception to be raised as 404.

Instructions as follows:

1) Visit /admin/structure/workbench-moderation/wb_404_redirection.
2) Check the states for which you want it to be redirected to 404.
3) Save configuration.
